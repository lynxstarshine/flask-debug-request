from flask import Flask, request, render_template
import jwt

from markupsafe import escape

import pprint

app = Flask(__name__)

@app.route("/")
def index():
    pp = pprint.PrettyPrinter(indent=4)

    jwt_token = "No token found in header"

    authorization_token = "No token found in header"

    if 'HTTP_AUTHORIZATION' in request.environ:
        try:
            authorization_token = jwt.decode(request.environ.get('HTTP_AUTHORIZATION').split(' ')[-1], options={"verify_signature": False})
        except jwt.PyJWTError as e:
            authorization_token = e

        authorization_token = escape(pp.pformat(authorization_token))

    if 'X-Auth-Request-Access-Token' in request.headers:
        try:
            jwt_token = jwt.decode(request.headers.get('X-Auth-Request-Access-Token'), options={"verify_signature": False})
            
        except jwt.PyJWTError as e:
            jwt_token = e

        jwt_token = escape(pp.pformat(jwt_token))

    formatted_headers = escape(pp.pformat(request.headers.__dict__))
    formatted_cookies = escape(pp.pformat(request.cookies.__dict__))

    return f"""
    <html>
    <head>
        <title>Testing app</title>
    </head>
    <body>
        <h1>Info</h1>
        <h2>Method</h2>
        <code>
            {request.method}
        </code>
        <h2>Remote address</h2>
        <div>{request.remote_addr}</div>
        <h2>Headers</h2>
        <pre>{formatted_headers}</pre>

        <h2>JWT Token from Autorization Header</h2>
        <pre>{authorization_token}</pre>

        <h2>JWT Token from X-Auth-Request-Access-Token</h2>
        <pre>{jwt_token}</pre>
        <h2>Cookies</h2>
        <pre>{formatted_cookies}</pre>
        <h2>Data</h2>
        <code>
            {escape(pp.pformat(request.data))}
        </code>
        <h2>Args</h2>
        <code>
            {escape(pp.pformat(request.args))}
        </code>
        <h2>Form</h2>
        <code>
            {escape(pp.pformat(request.form))}
        </code>

    </body>
</html>
    """


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
